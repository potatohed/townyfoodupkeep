package net.potatohed.TownyFoodUpkeep.Objects;

import org.bukkit.Material;

public class Upkeep 
{
	public Upkeep (Material material, int amount)
	{
		this.amount = amount;
		this.material = material;
		this.LAamount = amount;
		this.LRamount = 0;
	}
	public Upkeep ( String SerializedStr)
	{
		String str[] = SerializedStr.split(",");
		this.material = Material.getMaterial(str[0]);
		this.amount = Integer.parseInt(str[1]);
		this.LAamount = Integer.parseInt(str[2]);
		this.LRamount = Integer.parseInt(str[3]);
	}

	Material material;
	int amount;
	int LAamount; // Last Add Amount
	int LRamount; // Last Remove Amount

	public boolean add(int Amount)
	{
		try
		{
			this.amount += Amount;
			this.LAamount += Amount;
		}
		catch (Exception e)
		{
			return false;
		}
		return true;
	}
	public boolean remove(int Amount)
	{
		try
		{
			this.amount -= Amount;
			this.LRamount -= Amount;
		}
		catch (Exception e)
		{
			return false;
		}
		return true;

	}
	public boolean isoverdue()
	{
		if ( amount > (LAamount * 2))
			return true;
		else 
			return false;
	}
	public int amountoverdue ()
	{
		if ( amount > (LAamount * 2))
			return  amount - (LAamount * 2);
		else 
			return 0;
	}
	public int amount()
	{
		return amount;
	}
	public Material getMaterial()
	{
		return material;
	}
	public String toSerialString()
	{
		return material.toString() +","+ String.valueOf(amount) + "," + String.valueOf(LAamount) + "," + String.valueOf(LRamount);
	}
	public boolean resetLAmount()
	{
		LAamount = 0;
		LRamount = 0;
		return true;
	}

}
