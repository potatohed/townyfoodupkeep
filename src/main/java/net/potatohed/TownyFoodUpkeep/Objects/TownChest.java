package net.potatohed.TownyFoodUpkeep.Objects;

import java.util.LinkedList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.inventory.ItemStack;

import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.Town;
import com.palmergames.bukkit.towny.object.TownyUniverse;

public class TownChest
{
	public Town town;
	public List<Location> chestlist; 

	public TownChest(Town town)
	{
		this.town = town;
		chestlist = new LinkedList<Location>();
	}

	@SuppressWarnings("deprecation")
	public TownChest(String Serialstr, TownyUniverse tu) throws NotRegisteredException
	{
		String[] data = Serialstr.split("�");
		town = tu.getTown(data[0]);
		chestlist = new LinkedList<Location>();
		for (int i = 1; i < data.length; i++)
		{
			String[] indata = data[i].split(",");
			chestlist.add(new Location(Bukkit.getWorld(indata[0]), Double.parseDouble(indata[1]), Double.parseDouble(indata[2]), Double.parseDouble(indata[3])));
		}


	}
	public String toSerialString()
	{
		String returnstring = town.getName();
		for (Location loc : chestlist)
		{
			returnstring += "�" + loc.getWorld().getName() + "," + loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ();
		}
		return returnstring;
	}
	public boolean addchest(Location loc)
	{
		try
		{
			chestlist.add(loc);
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}
	public boolean removechest(Location loc)
	{
		return chestlist.remove(loc);
	}
	public Town getTown()
	{
		return town;
	}
	public int getAmount(Material material)
	{
		int amount = 0;
		for ( Location loc : chestlist)
		{
			for (ItemStack item : ((Chest) loc.getBlock().getState()).getInventory().getContents())
			{
				if(item != null && item.getType() == material)
					amount += item.getAmount();
			}
		}
		return amount;		
	}
	public boolean removeAmount(Material material,  int amount)
	{
		int left = amount;
		for ( Location loc : chestlist)
		{
			try
			{
			ItemStack cont[] = ((Chest) loc.getBlock().getState()).getInventory().getContents();
			ItemStack[] newcont = new ItemStack[cont.length];
			int index = 0;
			for ( ItemStack item : cont)
			{
				if (item != null && item.getType() == material)
				{
					if (item.getAmount() > left)
					{
						item.setAmount(item.getAmount() - left);
						left = 0;
						newcont[index] = item;
					} 
					else if (item.getAmount() == left)
					{
						left = 0;
					}
					else if (item.getAmount() < left)
					{
						left -= item.getAmount();						
					}						
				}
				else
				{
					newcont[index] = item;
				}
				index ++;
			}
			((Chest) loc.getBlock().getState()).getInventory().setContents(newcont);
			}
			catch(Exception e)
			{
				removechest(loc);
				Bukkit.getLogger().warning("Removing Chest at" + loc.toString());
			}
		}
		return (left == 0);

	}
	public boolean isTownChest(Location loc)
	{
		for(Location myloc : chestlist)
			if (myloc.equals(loc))
				return true;
		return false;
	}
}
