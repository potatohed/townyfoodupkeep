package net.potatohed.TownyFoodUpkeep.Runnables;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import com.palmergames.bukkit.towny.Towny;
import com.palmergames.bukkit.towny.object.TownyUniverse;

import net.potatohed.TownyFoodUpkeep.TownyFoodUpkeep;
import net.potatohed.TownyFoodUpkeep.Objects.TownChest;
import net.potatohed.TownyFoodUpkeep.Objects.TownUpkeep;
import net.potatohed.TownyFoodUpkeep.Objects.Upkeep;

public class UpkeepRemoveRunnable extends BukkitRunnable 
{
	TownyFoodUpkeep main;
	TownyUniverse tu;
	public UpkeepRemoveRunnable (Plugin plugin)
	{
		main = (TownyFoodUpkeep) plugin;
		tu = ((Towny) Bukkit.getPluginManager().getPlugin("Towny")).getTownyUniverse();
	}

	public void run() 
	{
		Bukkit.getLogger().info(main.ls.getLangFile().getString(this.getClass().getName() + ".Begin"));
		for(TownChest uc : main.TownChestList.values() )
		{ 
			try
			{
				TownUpkeep tu = main.getTownupkeep(uc.town);
				if (tu == null)
					return;
				for ( Upkeep uk : tu.getUpkeeps())
				{
					int amountfound = uc.getAmount(uk.getMaterial());
					int amountneeded = uk.amount();
					if (amountfound >= amountneeded)
					{
						uc.removeAmount(uk.getMaterial(), amountneeded);
						uk.remove(amountneeded);
					}
					else
					{
						uc.removeAmount(uk.getMaterial(), amountfound);
						uk.remove(amountfound);
					}
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

		}
		main.saveData();
	}
}
